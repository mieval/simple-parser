package main

import (
	"fmt"
	"html"
	"io/ioutil"
)

const cfName = "./json_responses.txt"

func main() {
	fileData := loadDataFromFile(cfName)
	fmt.Printf("%v", string(fileData))
}

func loadDataFromFile(fileName string) (data []byte) {
	data, err := ioutil.ReadFile(fileName)
	if err != nil {
		panic(err)
	}

	return

}

func decodeHtml(data string) string {
	return html.UnescapeString(string(data))
}
